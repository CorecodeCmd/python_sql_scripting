# CONDITIONAL EXPRESSIONS AND TRANSPOSING DATA (CASE CLAUSE)


#### Employees with salary under 100000 are considered as ‘Under Paid’, above 100000 but less than 160000 ‘Paid Well’,
#### above 160000 ‘Executive’ else ‘Unpaid’. Produce a report showing the total count of employees under the three categories 
#### ‘UNDER PAID’, ‘PAID WELL’, EXECUTIVE’

```sql
SELECT salary_category, COUNT(*) total_employees 
FROM (
 SELECT salary, 
 CASE
    WHEN salary < 100000 THEN 'UNDER PAID' 
    WHEN salary > 100000 AND salary < 160000 THEN 'PAID WELL'
    WHEN salary > 160000 THEN 'EXECUTIVE' 
    ELSE 'UNPAID'
 END AS salary_category
 FROM employees
 ORDER BY salary DESC
) salaries 
GROUP BY salary_category;

salary_category | total_employees
-----------------+-----------------
 UNDER PAID      |             581
 PAID WELL       |             382
 EXECUTIVE       |              37
 ```

#### Transpose the above data to have each row as column
```sql
SELECT 
SUM(CASE WHEN salary < 100000 THEN 1 ELSE 0 END) AS "UNDER PAID",
SUM(CASE WHEN salary > 100000 AND salary < 160000 THEN 1 ELSE 0 END) AS "PAID WELL",
SUM(CASE WHEN salary > 160000 THEN 1 ELSE 0 END) AS "EXECUTIVE"
 FROM employees;

UNDER PAID | PAID WELL | EXECUTIVE
------------+-----------+-----------
        581 |       382 |        37
```

OR (VERY EXPENSIVE QUERY)

```sql
SELECT 
SUM(CASE WHEN salary_category = 'UNDER PAID' THEN total_employees ELSE 0 END) AS "UNDER PAID",
SUM(CASE WHEN salary_category = 'PAID WELL' THEN total_employees ELSE 0 END) AS "PAID WELL",
SUM(CASE WHEN salary_category = 'EXECUTIVE' THEN total_employees ELSE 0 END) AS "EXECUTIVE"
FROM (
 SELECT salary_category, COUNT(*) total_employees 
 FROM (
 SELECT salary, 
 CASE
    WHEN salary < 100000 THEN 'UNDER PAID' 
    WHEN salary > 100000 AND salary < 160000 THEN 'PAID WELL'
    WHEN salary > 160000 THEN 'EXECUTIVE' 
    ELSE 'UNPAID'
 END AS salary_category
 FROM employees
 ORDER BY salary DESC
 ) salaries 
 GROUP BY salary_category
) b;

UNDER PAID | PAID WELL | EXECUTIVE
------------+-----------+-----------
        581 |       382 |        37
```


#### Return the number of employees in sports, tools, clothing and computer employees. Transpose sports, tools, 
#### clothing and computer employees as columns
```sql
SELECT 
SUM(CASE WHEN department = 'Sports' THEN 1 ELSE 0 END) AS "Sports Employees",
SUM(CASE WHEN department = 'Tools' THEN 1 ELSE 0 END) AS "Tools Employees",
SUM(CASE WHEN department = 'Clothing' THEN 1 ELSE 0 END) AS "Clothing Employees",
SUM(CASE WHEN department = 'Computers' THEN 1 ELSE 0 END) AS "Computers Employees"
FROM employees;
```

OR (Expensive query)

```sql
SELECT 
SUM(CASE WHEN department = 'Sports' THEN employees_count ELSE 0 END) AS "Sports Employees",
SUM(CASE WHEN department = 'Tools' THEN employees_count ELSE 0 END) AS "Tools Employees",
SUM(CASE WHEN department = 'Clothing' THEN employees_count ELSE 0 END) AS "Clothing Employees",
SUM(CASE WHEN department = 'Computers' THEN employees_count ELSE 0 END) AS "Computers Employees"
FROM (
  SELECT department, COUNT(*) AS employees_count
  FROM employees 
  WHERE department IN ('Sports', 'Tools', 'Clothing', 'Computers') 
  GROUP BY department
) emp_count;

Sports Employees | Tools Employees | Clothing Employees | Computers Employees
------------------+-----------------+--------------------+---------------------
               34 |              39 |                 49 |                  47
```

#### Generate an example report as below

```sql
first_name   |   region_1    |   region_2    |   region_3    | region_4 | region_5 | region_6 | region_7
----------------+---------------+---------------+---------------+----------+----------+----------+----------
 Berrie         |               |               |               | Asia     |          |          |
 Aeriell        |               |               | United States |          |          |          |
 Sydney         |               |               |               | Asia     |          |          |
 Avrom          |               |               |               |          |          |          | Canada
 Feliks         |               |               |               |          | Asia     |          |
 Bethena        |               |               | United States |          |          |          |
 Ardeen         |               |               |               |          |          |          | Canada
 Seline         |               |               | United States |          |          |          |
 Dayle          | United States |               |               |          |          |          |
 Redford        |               |               |               |          |          |          | Canada
 Nickey         |               |               |               |          |          |          | Canada
 Leonora        |               |               | United States |          |          |          |
 Anetta         | United States |               |               |          |          |          |
 Jodi           |               | United States |               |          |          |          |
 Alyson         |               |               |               |          |          | Canada   |
 Merell         |               |               |               |          |          |          | Canada
 Annora         |               |               |               |          | Asia     |          |
 Ronica         |               |               |               |          | Asia     |          |
 Bernardine     |               |               | United States |          |          |          |
 Jessey         |               |               |               |          |          |          | Canada
 Bernardo       |               |               |               |          |          | Canada   |
 Cayla          |               |               | United States |          |          |          |
 Vanda          |               |               |               | Asia     |          |          |
 Corabel        |               |               | United States |          |          |          |
 Zane           |               |               |               |          |          | Canada   |
 Frasquito      | United States |               |               |          |          |          |
 Niles          |               |               | United States |          |          |          |
 Emanuele       |               | United States |               |          |          |          |

SELECT first_name, 
CASE WHEN region_id = 1 THEN (SELECT country FROM regions WHERE region_id = 1) ELSE '' END AS region_1,
CASE WHEN region_id = 2 THEN (SELECT country FROM regions WHERE region_id = 2) ELSE '' END AS region_2,
CASE WHEN region_id = 3 THEN (SELECT country FROM regions WHERE region_id = 3) ELSE '' END AS region_3,
CASE WHEN region_id = 4 THEN (SELECT country FROM regions WHERE region_id = 4) ELSE '' END AS region_4,
CASE WHEN region_id = 5 THEN (SELECT country FROM regions WHERE region_id = 5) ELSE '' END AS region_5,
CASE WHEN region_id = 6 THEN (SELECT country FROM regions WHERE region_id = 6) ELSE '' END AS region_6,
CASE WHEN region_id = 7 THEN (SELECT country FROM regions WHERE region_id = 7) ELSE '' END AS region_7
FROM employees;
```

#### Return the count of employees that come from countries United States, Asia, and Canada
```sql
SELECT region_1 + region_2 + region_3 AS "United States", region_4 + region_5 AS "Asia",
region_6 + region_7 AS "Canada"
FROM (
  SELECT 
  SUM(CASE WHEN region_id = 1 THEN 1 ELSE 0 END) AS region_1,
  SUM(CASE WHEN region_id = 2 THEN 1 ELSE 0 END) AS region_2,
  SUM(CASE WHEN region_id = 3 THEN 1 ELSE 0 END) AS region_3,
  SUM(CASE WHEN region_id = 4 THEN 1 ELSE 0 END) AS region_4,
  SUM(CASE WHEN region_id = 5 THEN 1 ELSE 0 END) AS region_5,
  SUM(CASE WHEN region_id = 6 THEN 1 ELSE 0 END) AS region_6,
  SUM(CASE WHEN region_id = 7 THEN 1 ELSE 0 END) AS region_7
  FROM employees
) country_regions;

United States | Asia | Canada
---------------+------+--------
           438 |  264 |    298
```