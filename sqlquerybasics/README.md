# SQL QUERY BASICS (WHERE, OR, AND, BETWEEN, LIMIT, DISTINCT, LIKE, IS, NOT )

#### Return the first_name and email of females that work in the tools department having a salary greater than 110,000
```sql
SELECT first_name, email 
FROM employees WHERE gender = 'F' 
AND department = 'Tools' 
AND salary > 110000; 
```


#### Return the first_name and hire_date of those employees who earn more than 165,000 as well as those employees that work in the sports department
#### and also happen to be male
```sql
SELECT first_name, hire_date 
FROM employees 
WHERE salary > 165000 
OR (department = 'Sports' AND gender = 'M'); 
```

#### Return the first names and hire dates of those employees who were hired during Jan 1st 2002 and Jan 1st 2004
```sql
SELECT first_name, hire_date 
FROM employees 
WHERE hire_date BETWEEN '2002-01-01' AND '2004-01-01'; 
```


#### Return the male employees who work in the automotive department and earn more than 40,000 and less than 100,000 as well as females that work in the toys department
```sql
SELECT * 
FROM employees 
WHERE gender = 'M' 
AND department = 'Automotive' 
AND salary > 40000 AND salary < 100000 
OR (gender = 'F' AND department = 'Toys')  
ORDER BY gender DESC 
```

#### Return distinct employee departments
```sql
SELECT DISTINCT department FROM employees;
```


#### Return all those students that contain the letters “ch” in their name or their name ends with the letters “nd”
```sql
SELECT *
FROM students
WHERE student_name LIKE '%ch%'
OR student_name LIKE '%nd';
```

#### Return the name of those students that have letters “ae” or “ph” in their name and are NOT 19 years old.
```sql
SELECT student_name
FROM students
WHERE (student_name LIKE '%ae%'
OR student_name LIKE '%ph%')
AND age <> 19;
```

#### Return the names and ages of the top 4 oldest students
```sql
SELECT student_name, age
FROM students
ORDER BY age DESC
LIMIT 4;
```

OR

```sql
SELECT student_name, age
FROM students
ORDER BY age DESC
FETCH FIRST 4 ROWS ONLY;
```

LIMIT 10 == FETCH FIRST 10 ROWS ONLY

#### Return students based on the following criteria:
The student must not be older than age 20 if their student_no is either between 3 and 5 or their student_no is 7.
Your query should also return students older than age 20 but in that case they must have a student_no that is at least 4

```sql
SELECT *
FROM students
WHERE ((student_no BETWEEN 3 AND 5 OR student_no = 7)
AND age <= 20)
OR (age > 20 and student_no >= 4);
```