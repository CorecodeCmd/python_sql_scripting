import click

from beautifultable import BeautifulTable

from database.db_connection import DbHandler

table = BeautifulTable()
db = DbHandler()


query_commands = (
    {
        "QUERY_FEMALES_SALARY_GREATER_110000": """
            SELECT first_name, email 
            FROM employees WHERE gender = 'F' 
            AND department = 'Tools' 
            AND salary > 110000
        """
    },
    {
        "QUERY_HIRES_EARN_MORE_165000": """
            SELECT first_name, hire_date 
            FROM employees 
            WHERE salary > 165000 
            OR (department = 'Sports' AND gender = 'M')
        """
    },
    {
        "QUERY_HIRES_BETWEEN_1st-2002-AND_Jan-1st-2004": """
            SELECT first_name, hire_date 
            FROM employees 
            WHERE hire_date BETWEEN '2002-01-01' AND '2004-01-01'
        """
    },
    {
        "QUERY_UNIQUE_EMPLOYEE_DEPARTMENTS": """
            SELECT DISTINCT department FROM employees
        """
    },
    {
        "QUERY_TOP_4_OLDEST_STUDENTS": """
            SELECT student_name, age
            FROM students
            ORDER BY age DESC
            FETCH FIRST 4 ROWS ONLY
        """
    }
)

sample_analytics_problems = (
    'Return the first_name and email of females that work in the tools department having a salary greater than 110,000',
    'Return the first_name and hire_date of those employees who earn more than 165,000 as well as those employees that work in the sports department \
     and also happen to be male',
    'Return the first names and hire dates of those employees who were hired during Jan 1st 2002 and Jan 1st 2004',
    'Return distinct employee departments',
    'Return the names and ages of the top 4 oldest students'
)


def execute_queries():
    
    with db.connection() as conn:
        with conn.cursor() as cursor:
            click.echo(click.style('SELECT FROM  THE QUERIES BELOW',
                                    fg='red', bold=True))
            menu = 'select_problem'
            while True:
                if menu == 'select_problem':
                    for index, value in enumerate(sample_analytics_problems):
                        click.echo(click.style(f"   {index + 1}:  {value}", fg="blue"))
                    click.echo(click.style("   BACK(b):  Return to Main Menu", fg="blue"))
                    char = click.getchar()
                    if char == '1':
                        click.clear()
                        sql = query_commands[0]['QUERY_FEMALES_SALARY_GREATER_110000'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["first_name", "email"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '2':
                        click.clear()
                        sql = query_commands[1]['QUERY_HIRES_EARN_MORE_165000'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["first_name", "hire_date"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '3':
                        click.clear()
                        sql = query_commands[2]['QUERY_HIRES_BETWEEN_1st-2002-AND_Jan-1st-2004'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["first_name", "hire_date"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '4':
                        click.clear()
                        sql = query_commands[3]['QUERY_UNIQUE_EMPLOYEE_DEPARTMENTS'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["departments"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '5':
                        click.clear()
                        sql = query_commands[4]['QUERY_TOP_4_OLDEST_STUDENTS'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["student_name", "age"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == 'b':
                        click.clear()
                        menu = 'Back'
                elif menu == 'Back':
                    return
