import click

from database.database_manager import CreateTables
from database.load_sample_data import LoadData
from sqlquerybasics.query_basics import execute_queries
from sqlfunctions.sql_functions import sql_functions
from group_by_aggregate_functions.aggregates import sql_aggregate_functions
from subqueries.sub_query import sql_sub_queries

tables = CreateTables()
table_data = LoadData()


@click.group()
def cli():
    click.echo("SQL Tool launching")


@cli.command()
def initdb():
    """
    Create Database tables
    """
    click.echo(f"Initialising database and create tables")
    tables.create_all()


@cli.command()
def load_data():
    """
    Load sample data into database tables
    """
    click.echo("Truncating table data")
    tables.truncate_all()
    click.echo("Loading sample data into database tables")
    table_data.sample_data()


@cli.command()
def truncate_tables():
    """
    Truncate database tables
    """
    click.echo("Truncating database tables")
    tables.truncate_all()


@cli.command()
def menu():
    """
    Show a simple menu
    """
    menu = 'main'
    while True:
        if menu == 'main':
            click.echo("Main Menu")
            click.echo("   1:  Execute queries")
            click.echo("   2:  SQL Functions")
            click.echo("   3:  SQL Aggregate Functions")
            click.echo("   4:  SQL Sub Queries")
            click.echo("   q:  Quit")
            char = click.getchar()
            if char == '1':
                click.clear()
                execute_queries()
            elif char == '2':
                click.clear()
                sql_functions()
            elif char == '3':
                click.clear()
                sql_aggregate_functions()
            elif char == '4':
                click.clear()
                sql_sub_queries()
            elif char == 'q':
                menu = 'quit'
        elif menu == 'quit':
            return
