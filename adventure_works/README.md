# AdventureWorks

SQL Queries To Answer Questions on the [Adventure Works Database](https://sqlzoo.net/wiki/AdventureWorks) from the SQLZoo.net



## [Easy Questions](https://sqlzoo.net/wiki/AdventureWorks_easy_questions)

#### 1. Show the first name and the email address of customer with CompanyName 'Bike World'
```sql
SELECT FirstName, EmailAddress 
FROM Customer 
WHERE CompanyName = 'Bike World'
```

#### 2. Show the CompanyName for all customers with an address in City 'Dallas'.
```sql
SELECT DISTINCT CompanyName 
FROM Customer cu
JOIN CustomerAddress ca ON cu.CustomerID = ca.CustomerID
JOIN Address ad ON ca.AddressID = ad.AddressID
WHERE ad.City = 'Dallas'
```

#### 3. How many items with ListPrice more than $1000 have been sold?
```sql
SELECT COUNT(p.ProductID) items
FROM Product p 
JOIN SalesOrderDetail s ON p.ProductID = s.ProductID
WHERE ListPrice > 1000
```

#### 4. Give the CompanyName of those customers with orders over $100000. Include the subtotal plus tax plus freight.
```sql
SELECT CompanyName, (SubTotal + TaxAmt + Freight) AS orders
FROM Customer c
JOIN SalesOrderHeader s ON c.CustomerID = s.CustomerID
WHERE (SubTotal + TaxAmt + Freight) > 100000
```

#### 5. Find the number of left racing socks ('Racing Socks, L') ordered by CompanyName 'Riding Cycles'
```sql
SELECT COUNT(*) racing_socks FROM Product p
JOIN SalesOrderDetail sod ON p.ProductID = sod.ProductID
JOIN SalesOrderHeader soh ON sod.SalesOrderID = soh.SalesOrderID
JOIN Customer c ON soh.CustomerID = c.CustomerID
WHERE Name = 'Racing Socks, L' 
AND c.CompanyName = 'Riding Cycles'
```

## [Medium Questions](https://sqlzoo.net/wiki/AdventureWorks_medium_questions)

#### 6. A "Single Item Order" is a customer order where only one item is ordered. Show the SalesOrderID and the UnitPrice for every Single Item Order.
```sql
SELECT SalesOrderID, UnitPrice
FROM SalesOrderDetail
WHERE OrderQty = 1
```

#### 7. Where did the racing socks go? List the product name and the CompanyName for all Customers who ordered ProductModel 'Racing Socks'.
```sql
SELECT pm.Name AS "product name", CompanyName
FROM ProductModel pm
JOIN Product p ON pm.ProductModelID = p.ProductModelID
JOIN SalesOrderDetail sod ON p.ProductID = sod.ProductID
JOIN SalesOrderHeader soh ON sod.SalesOrderID = soh.SalesOrderID
JOIN Customer c ON soh.CustomerID = c.CustomerID
WHERE pm.Name = 'Racing Socks'
```

#### 8. Show the product description for culture 'fr' for product with ProductID 736.
```sql
SELECT pd.Description 
FROM ProductDescription pd
JOIN ProductModelProductDescription pmd ON pd.ProductDescriptionID = pmd.ProductDescriptionID
JOIN ProductModel pm ON pmd.ProductModelID = pm.ProductModelID
JOIN Product p ON pm.ProductModelID = p.ProductModelID
WHERE pmd.Culture = 'fr'
AND p.ProductID = 736
```

#### 9. Use the SubTotal value in SaleOrderHeader to list orders from the largest to the smallest. For each order show the CompanyName and the SubTotal and the total weight of the order.
```sql
SELECT CompanyName, SubTotal,  OrderQty * Weight AS "Total Weight"
FROM Product p
JOIN SalesOrderDetail sod ON p.ProductID = sod.ProductID
JOIN SalesOrderHeader soh ON sod.SalesOrderID = soh.SalesOrderID
JOIN Customer c ON soh.CustomerID = c.CustomerID
ORDER BY soh.SubTotal DESC
```

#### 10. How many products in ProductCategory 'Cranksets' have been sold to an address in 'London'?
```sql
SELECT COUNT(*) AS product_count 
FROM ProductCategory pc
JOIN Product p ON pc.ProductCategoryID = p.ProductCategoryID
JOIN SalesOrderDetail sod ON p.ProductID = sod.ProductID
JOIN SalesOrderHeader soh ON sod.SalesOrderID = soh.SalesOrderID
JOIN Address a ON soh.ShipToAddressID = a.AddressID
WHERE pc.Name = 'Cranksets' 
AND a.City = 'London'
```

## [Hard Questions](https://sqlzoo.net/wiki/AdventureWorks_hard_questions)

#### 11. For every customer with a 'Main Office' in Dallas show AddressLine1 of the 'Main Office' and AddressLine1 of the 'Shipping' address - if there is no shipping address leave it blank. Use one row per customer.
```sql
WITH Dallas_customers AS (
    SELECT c.CustomerID, CompanyName, AddressType, AddressLine1, City 
    FROM Customer c
   JOIN CustomerAddress ca ON c.CustomerID = ca.CustomerID
   JOIN Address a ON ca.AddressID = a.AddressID
   WHERE a.City = 'Dallas'
)

SELECT DISTINCT CustomerID, CompanyName, AddressLIne1 AS "Main Address", 
 COALESCE((SELECT AddressLine1 FROM Dallas_customers b
    WHERE a.CompanyName = b.CompanyName 
    AND b.AddressType = 'Shipping'), ' ') AS "Shipping Address"
FROM Dallas_customers a
WHERE a.AddressType = 'Main Office'
```

#### 12. For each order show the SalesOrderID and SubTotal calculated three ways: 
#### A) From the SalesOrderHeader 
#### B) Sum of OrderQty*UnitPrice 
#### C) Sum of OrderQty*ListPrice
```sql
SELECT soh.SalesOrderID, SUM(SubTotal) SubTotal, SUM(OrderQty * UnitPrice), SUM(OrderQty * ListPrice)
FROM Customer c
JOIN SalesOrderHeader soh ON c.CustomerID = soh.CustomerID
JOIN SalesOrderDetail sod ON soh.SalesOrderID = sod.SalesOrderID
JOIN Product p ON sod.ProductID = p.ProductID
GROUP BY soh.SalesOrderID
```

#### 13. Show the best selling item by value.
```sql
SELECT p.Name, SUM(OrderQty) Quantity_sold
FROM Customer c
JOIN SalesOrderHeader soh ON c.CustomerID = soh.CustomerID
JOIN SalesOrderDetail sod ON soh.SalesOrderID = sod.SalesOrderID
JOIN Product p ON sod.ProductID = p.ProductID
GROUP BY p.Name
ORDER BY SUM(OrderQty) DESC
LIMIT 1
```

#### 14. Show how many orders are in the following ranges (in $):
```
RANGE      Num Orders      Total Value
    0-  99
  100- 999
 1000-9999
10000-
```

```sql
WITH Order_Ranges AS (
  SELECT p.ProductID, (SubTotal + TaxAmt + Freight) AS amount, 
  CASE 
    WHEN (SubTotal + TaxAmt + Freight)  BETWEEN 0 AND 100 THEN '0- 99'
    WHEN (SubTotal + TaxAmt + Freight)  BETWEEN 100 AND 1000 THEN '100- 999'
    WHEN (SubTotal + TaxAmt + Freight)  BETWEEN 1000 AND 10000 THEN '1000- 9999'
    WHEN (SubTotal + TaxAmt + Freight)  > 10000 THEN '10000- '
  END AS "Ranges"
  FROM Customer c
 JOIN SalesOrderHeader soh ON c.CustomerID = soh.CustomerID
 JOIN SalesOrderDetail sod ON soh.SalesOrderID = sod.SalesOrderID
 JOIN Product p ON sod.ProductID = p.ProductID
 ORDER BY p.ProductID
)

SELECT Ranges AS "Range", COUNT(*) AS "Num Orders", SUM(amount) AS "Total Value"
FROM Order_Ranges
GROUP BY Ranges
ORDER BY Ranges
```

#### 15. Identify the three most important cities. Show the break down of top level product category against city.
```sql
SELECT City, pc.Name ProductCategory, SUM(OrderQty * UnitPrice) AS SalesSum
FROM ProductCategory pc
JOIN Product p ON pc.ProductCategoryID = p.ProductCategoryID
JOIN SalesOrderDetail sod ON p.ProductID = sod.ProductID
JOIN SalesOrderHeader soh ON sod.SalesOrderID = soh.SalesOrderID
JOIN Customer c ON soh.CustomerID = c.CustomerID
JOIN CustomerAddress ca ON c.CustomerID = ca.CustomerID
JOIN Address ad ON ca.AddressID = ad.AddressID
GROUP BY City, pc.Name
ORDER BY SUM(OrderQty * UnitPrice) DESC
LIMIT 3
```
