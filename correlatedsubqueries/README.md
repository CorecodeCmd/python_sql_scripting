# CORRELATED SUBQUERIES

Correlated query is a query nested with in another query and uses results of the outer query

#### Return employees and their salary who earn more than their departmental average salary
```sql
SELECT first_name, salary 
FROM employees e1 
WHERE salary > (SELECT AVG(salary)
                FROM employees e2 
                WHERE e1.department = e2.department);
```

NB:  The subquery portion runs for every single record of the outer query(repeating)

#### Return employees and their salary who earn more than their regional average salary
```sql
SELECT first_name, salary 
FROM employees e1 
WHERE salary > (SELECT AVG(salary)
                FROM employees e2 
                WHERE e1.region_id = e2.region_id);
```

#### Return the employees name, salary, department and their departmental avg_salary for their respective departments
```sql
SELECT first_name, department, salary, 
  (SELECT ROUND(AVG(salary))
   FROM employees e2 
   WHERE e1.department = e2.department) avg_dept_salary
FROM employees e1;
```

#### Return departments that have more than 35 employees working

With correlated subquery
```sql
SELECT department, 
 (SELECT COUNT(*) FROM employees e2
  WHERE d.department = e2.department) employees_count
FROM departments d 
WHERE (SELECT COUNT(*) FROM employees e2
       WHERE d.department = e2.department) > 35;
```

OR

```sql
SELECT DISTINCT department, 
 (SELECT COUNT(*) FROM employees e2
  WHERE e1.department = e2.department) employees_count
FROM employees e1 
WHERE (SELECT COUNT(*) FROM employees e2
       WHERE e1.department = e2.department) > 35;
``` 

Using GROUP BY AND HAVING Clause
```sql
SELECT department, COUNT(*) employee_count
FROM employees 
GROUP BY department 
HAVING COUNT(*) > 35;

department     | employees_count
-------------------+-----------------
 Beauty            |              45
 Books             |              37
 Camping           |              36
 Children Clothing |              47
 Clothing          |              49
 Computers         |              47
 Decor             |              39
 Device Repair     |              51
 First Aid         |              58
 Furniture         |              43
 Games             |              36
 Garden            |              41
 Jewelry           |              41
 Movies            |              56
 Pharmacy          |              38
 Tools             |              39
 Toys              |              47
 Vitamins          |              37
```

#### Return departments and highest employees salary number per department that have more than 35 employees working 
```sql
SELECT department, 
 (SELECT MAX(salary) FROM employees e2
  WHERE d.department = e2.department) max_dept_salary
FROM departments d 
WHERE (SELECT COUNT(*) FROM employees e2
       WHERE d.department = e2.department) > 35;

department     | max_dept_salary
-------------------+-----------------
 Clothing          |          166976
 Decor             |          153849
 Furniture         |          160170
 Computers         |          163512
 Device Repair     |          164355
 Garden            |          163361
 Children Clothing |          158546
 Toys              |          163688
 Vitamins          |          163933
 Pharmacy          |          166016
 First Aid         |          164011
 Tools             |          156672
 Jewelry           |          163794
 Beauty            |          162845
 Books             |          159561
 Games             |          152615
 Movies            |          159463
```