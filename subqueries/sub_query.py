import click

from beautifultable import BeautifulTable

from database.db_connection import DbHandler

table = BeautifulTable()
db = DbHandler()


query_commands = (
    {
        "QUERY_EMPLOYEES_IN_ELECTRONICS_DIVISION": """
            SELECT * 
            FROM employees 
            WHERE department IN (
            SELECT department FROM departments 
            WHERE division = 'Electronics') 
            LIMIT 10
        """
    },
    {
        "QUERY_EMPLOYEE_IN_KIDS_HIRED_EALIER_THAN_MAINTENANCE": """
            SELECT * 
            FROM employees 
            WHERE department = ANY (
            SELECT department FROM departments 
            WHERE division = 'Kids') 
            AND hire_date > ALL (
            SELECT hire_date FROM employees 
            WHERE department = 'Maintenance') 
            LIMIT 10
        """
    },
    {
        "QUERY_AVGERAGE_SALARY": """
            SELECT ROUND(AVG(salary)) avg_salary 
            FROM employees
            WHERE salary NOT IN (
            (
                SELECT MAX(salary)
                FROM employees
            ),
            (
                SELECT MIN(salary)
                FROM employees
            ));
        """
    },
)

sample_analytics_problems = (
    'Return all employees that work in the electronics division',
    'Return all of those employees that work in the kids division AND the dates at which those employees were hired is greater than \
     all of the hire_dates of employees who work in the maintenance department',
    'Compute the average salary of employees excluding the highly and less paid employees',
)


def sql_sub_queries():
    
    with db.connection() as conn:
        with conn.cursor() as cursor:
            click.echo(click.style('SELECT FROM  THE QUERIES BELOW',
                                    fg='red', bold=True))
            menu = 'select_problem'
            while True:
                if menu == 'select_problem':
                    for index, value in enumerate(sample_analytics_problems):
                        click.echo(click.style(f"   {index + 1}:  {value}", fg="blue"))
                    click.echo(click.style("   BACK(b):  Return to Main Menu", fg="blue"))
                    char = click.getchar()
                    if char == '1':
                        click.clear()
                        sql = query_commands[0]['QUERY_EMPLOYEES_IN_ELECTRONICS_DIVISION'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["employee_id", "first_name", "last_name", "email", "hire_date", "department", "gender", "salary", "region_id"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '2':
                        click.clear()
                        sql = query_commands[1]['QUERY_EMPLOYEE_IN_KIDS_HIRED_EALIER_THAN_MAINTENANCE'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["employee_id", "first_name", "last_name", "email", "hire_date", "department", "gender", "salary", "region_id"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '3':
                        click.clear()
                        sql = query_commands[2]['QUERY_AVGERAGE_SALARY'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["average salary"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == 'b':
                        click.clear()
                        menu = 'Back'
                elif menu == 'Back':
                    return