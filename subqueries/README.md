# SQL SUB_QUERIES (ANY And ALL)

#### Return all employees that work in the electronics division
```sql
SELECT * 
FROM employees 
WHERE department IN (
 SELECT department FROM departments 
 WHERE division = 'Electronics'
);
```

#### Return all employees that earn more than 130000 dollars and work in either Asia or Canada
```sql
SELECT * 
FROM employees 
WHERE salary > 130000 
AND region_id IN (
 SELECT region_id FROM regions 
 WHERE country IN ('Asia', 'Canada')
);
```

#### Return first_name, department and how much less they make than the highly paid employ in the company and they work in either Asia or Canada
```sql
SELECT first_name, department, (SELECT MAX(salary) FROM employees a) - salary AS less_pay_than_highly_paid 
FROM employees b
WHERE region_id IN (
 SELECT region_id FROM regions 
 WHERE country IN ('Asia', 'Canada')
);
```

#### Return all of those employees that work in the kids division AND the dates at which those employees were hired is greater than 
#### all of the hire_dates of employees who work in the maintenance department
```sql
SELECT * 
FROM employees 
WHERE department IN (
 SELECT department FROM departments 
 WHERE division = 'Kids') 
AND hire_date > ALL (
 SELECT hire_date FROM employees 
 WHERE department = 'Maintenance'
);
```

OR
```sql
SELECT * 
FROM employees 
WHERE department = ANY (
 SELECT department FROM departments 
 WHERE division = 'Kids') 
AND hire_date > ALL (
 SELECT hire_date FROM employees 
 WHERE department = 'Maintenance'
);
```

#### Return the salary of the employees that appears most frequently
```sql
 SELECT salary
 FROM employees
 GROUP BY salary
 HAVING COUNT(*) >= ALL(
  SELECT COUNT(*) FROM employees
  GROUP BY salary
 )
 ORDER BY salary DESC 
 LIMIT 1;
```

OR

```sql
SELECT salary AS highest_freq_salary
FROM (
 SELECT salary, COUNT(*) AS freq
 FROM employees 
 GROUP BY salary
 ORDER BY COUNT(*) DESC, salary DESC
 LIMIT 1
) b;
```

#### Compute the average salary of employees excluding the highly and less paid employees
```sql
SELECT ROUND(AVG(salary)) avg_salary 
FROM employees
WHERE salary <> (
 SELECT MAX(salary)
 FROM employees
)
AND salary <> (
  SELECT MIN(salary)
  FROM employees
);
```

OR

```sql
SELECT ROUND(AVG(salary)) avg_salary 
FROM employees
WHERE salary NOT IN (
(
 SELECT MAX(salary)
 FROM employees
),
(
  SELECT MIN(salary)
  FROM employees
));
```

#### Removing Redundant Data
```sql
SELECT MIN(id) id, name
FROM dupes
GROUP BY name
ORDER BY id;
 id |  name
----+--------
  1 | FRANK
  3 | ROBERT
  5 | SAM
  7 | PETER
```

OR

```sql
SELECT MAX(id) id, name
FROM dupes
GROUP BY name
ORDER BY id;
id |  name
----+--------
  4 | ROBERT
  5 | SAM
  6 | FRANK
  7 | PETER
```

OR

```sql
SELECT * FROM dupes 
WHERE id IN (
 SELECT MIN(id) id
 FROM dupes
 GROUP BY name
 ORDER BY id
);
```

```sql
DELETE FROM dupes 
WHERE id NOT IN (
 SELECT MIN(id) id
 FROM dupes
 GROUP BY name
 ORDER BY id
);
```
