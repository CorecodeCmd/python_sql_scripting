from .db_connection import DbHandler


class CreateTables(DbHandler):
    def create_all(self):
        """create tables in the sql_tool database"""

        commands = (
            """
            CREATE TABLE IF NOT EXISTS departments (
                department varchar(100),
                division varchar(100),
                primary key (department)
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS regions (
                region_id int,
                region varchar(20),
                country varchar(20),
                primary key (region_id)
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS employees (
                employee_id INT,
                first_name VARCHAR(50),
                last_name VARCHAR(50),
                email VARCHAR(50),
                hire_date DATE,
                department VARCHAR(17),
                gender VARCHAR(1),
                salary INT,
                region_id INT,
                primary key (employee_id)
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS students
            (
                student_no integer,
                student_name varchar(20),
                age integer
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS courses
            (
                course_no varchar(5),
                course_title varchar(20),
                credits integer
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS student_enrollment
            (
                student_no integer,
                course_no varchar(5)
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS professors
            (
                last_name varchar(20),
                department varchar(12),
                salary integer,
                hire_date date
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS teach
            (
                last_name varchar(20),
                course_no varchar(5)
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS sales
            (
                continent varchar(20),
                country varchar(20),
                city varchar(20),
                units_sold integer
            )
            """,
        )

        with self.connection() as conn:
            with conn.cursor() as cursor:
                for command in commands:
                    cursor.execute(command)
            conn.commit()
        print("Done creating database tables")

    def truncate_all(self):
        """Truncate tables in the database"""

        commands = (
            """
            TRUNCATE TABLE departments RESTART IDENTITY
            """,
            """
            TRUNCATE TABLE regions RESTART IDENTITY
            """,
            """
            TRUNCATE TABLE employees RESTART IDENTITY
            """,
            """
            TRUNCATE TABLE sales RESTART IDENTITY
            """,
        )

        with self.connection() as conn:
            with conn.cursor() as cursor:
                for command in commands:
                    cursor.execute(command)
            conn.commit()
