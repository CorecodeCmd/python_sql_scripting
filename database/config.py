import os
import urllib.parse as urlparse
from configparser import ConfigParser


class DbParser:
    def config(self, filename=None, section="postgresql"):
        db = {}
        if filename:
            parser = ConfigParser()
            parser.read(filename)

            if parser.has_section(section):
                params = parser.items(section)
                for param in params:
                    db[param[0]] = param[1]
            else:
                raise Exception(
                    "Section {0} not found in file {1}".format(section, filename)
                )
        else:
            params = {}
            database_url = urlparse.urlparse(os.getenv("DATABASE_URL"))
            params["database"] = database_url.path[1:]
            params["user"] = database_url.username
            params["password"] = database_url.password
            params["host"] = database_url.hostname
            params["port"] = database_url.port
            for param in params:
                db[param[0]] = param[1]
        return db
