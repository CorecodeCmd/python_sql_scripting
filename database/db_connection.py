import os
import psycopg2

from .config import DbParser


class DbHandler:
    def connection(self):
        """Connect to the PostgreSQL database server"""
        conn = None

        try:
            filename = "database.ini"
            parser = DbParser()
            params = parser.config(filename=filename)
            conn = psycopg2.connect(**params)
            return conn
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
