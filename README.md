# MINI SQL ANALYTICS TOOL 

### Tool set up

clone project
```
$ git clone https://CorecodeCmd@bitbucket.org/CorecodeCmd/python_sql_scripting.git
$ cd python_sql_scripting
```

Install virtual environment
```
$ pip install virtualenv
$ virtualenv venv
```

Activate virtual environment

On Mac/Linux
```
$ venv/bin/activate
```

On windows
```
$ venv/scripts/activate
```


### Setting up database

To set up this tool install [postgres](https://www.postgresql.org/download/macosx/) instance on your machine

Rename example example.database.ini to database.ini and provide the database parameters under the postgresql section

Install the tool application
```
$ pip install --editable .
```

Initialise database and create tables
```
$ sql_tool initdb
```

Load sample data into database tables
```
$ sql_tool load-data
```
