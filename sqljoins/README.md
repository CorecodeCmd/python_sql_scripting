# SQL JOINS

#### Return the first name, email address of the employee, the country and the division in which the employee works in and make sure there is an email address.
```sql
SELECT first_name, email, division, country
FROM employees e, departments d, regions r
WHERE e.department = d.department 
AND e.region_id = r.region_id
AND email IS NOT NULL

first_name |           email            |   division    |    country
------------+----------------------------+---------------+---------------
 Berrie     | bmanueau0@dion.ne.jp       | Outdoors      | Asia
 Aeriell    | amcnee1@google.es          | Hardware      | United States
 Sydney     | ssymonds2@hhs.gov          | Home          | Asia
 Feliks     | fmorffew4@a8.net           | Electronics   | Asia
 Bethena    | btrow5@technorati.com      | Outdoors      | United States
 Ardeen     | acurwood6@1und1.de         | Home          | Canada
 Seline     | sdubber7@t-online.de       | Electronics   | United States
 Dayle      | dtrail8@tamu.edu           | Health        | United States
 Nickey     | npointona@vistaprint.com   | Fashion       | Canada
 Leonora    | lcasarolib@plala.or.jp     | Fashion       | United States
 Jodi       | jhookd@booking.com         | Hardware      | United States
 Merell     | myakovlivf@ucsd.edu        | Entertainment | Canada
 Annora     | abendelowg@google.com.hk   | Kids          | Asia
 Bernardine | bhendricksi@privacy.gov.au | Electronics   | United States
 Jessey     | jcolumj@pen.io             | Electronics   | Canada
 Vanda      | vmarwickm@upenn.edu        | Hardware      | Asia
 Corabel    | csyversenn@aboutads.info   | Health        | United States
 Zane       | zbreemo@abc.net.au         | Fashion       | Canada
 Niles      | nchawkleyq@flavors.me      | Entertainment | United States
 Emanuele   | esandcraftr@toplist.cz     | Outdoors      | United States
```

#### Return country and the total number of employees employed in that country
```sql
SELECT country, COUNT(*) employees_count
FROM employees e, regions r 
WHERE e.region_id = r.region_id
GROUP BY country;

country    | employees_count
---------------+-----------------
 United States |             438
 Canada        |             298
 Asia          |             264
```

#### INNER AND OUTER JOIN

FROM the first above problem
```sql
SELECT first_name, country
FROM employees e
INNER JOIN regions r
ON e.region_id = r.region_id;


SELECT first_name, email, division, country
FROM employees e
INNER JOIN departments d 
ON e.department = d.department 
INNER JOIN regions r
ON e.region_id = r.region_id
WHERE email IS NOT NULL;
```

#### Return departments that exist only in the employees table but not in the departments table.
```sql
SELECT DISTINCT e.department, d.department 
FROM employees e
LEFT OUTER JOIN departments d
ON e.department = d.department
WHERE d.department IS NULL;

department  | department
-------------+------------
 Camping     |
 Maintenance |
 Plumbing    |
 Security    |
```

#### UNOIN, UNION ALL AND EXCEPT
```sql
SELECT department FROM employees
UNION                  --- UNION gets results from top query stacking them with down query but removing duplicates
SELECT department FROM departments


SELECT DISTINCT department FROM employees
UNION ALL                  --- UNION ALL gets results from top query stacking them with down query but not removing duplicates
SELECT department FROM departments
```

#### EXCEPT
```sql
SELECT DISTINCT department FROM employees
EXCEPT                  --— EXCEPT returns rows only in the employees table
SELECT department FROM departments
```

#### Generate report showing a breakdown per department (Showing the count of employees in a department) and the total number of employees at the bottom
```sql
SELECT department, COUNT(*) employee_count 
FROM employees 
GROUP BY department
UNION ALL
SELECT 'TOTAL', COUNT(*) 
FROM employees;

department     | employee_count
-------------------+----------------
 Pharmacy          |             38
 Children Clothing |             47
 Camping           |             36
 Games             |             36
 Beauty            |             45
 Computers         |             47
 Tools             |             39
 Maintenance       |              8
 Cosmetics         |             34
 Device Repair     |             51
 Furniture         |             43
 Vitamins          |             37
 Sports            |             34
 Plumbing          |              7
 Decor             |             39
 Books             |             37
 Clothing          |             49
 Music             |             29
 Jewelry           |             41
 Automotive        |             32
 Garden            |             41
 Toys              |             47
 Security          |              6
 Grocery           |             28
 Movies            |             56
 Phones & Tablets  |             35
 First Aid         |             58
 TOTAL             |           1000
```

#### Return the first name, department, hire date and country of the first and last employees hired in the company.
```sql
SELECT first_name, department, hire_date, country
FROM employees e 
JOIN regions r ON e.region_id = r.region_id 
WHERE hire_date = (SELECT MIN(hire_date) FROM employees e2)
UNION ALL
SELECT first_name, department, hire_date, country
FROM employees e 
JOIN regions r ON e.region_id = r.region_id 
WHERE hire_date = (SELECT MAX(hire_date) FROM employees e3);

first_name | department | hire_date  | country
------------+------------+------------+---------
 Norbie     | First Aid  | 2003-01-01 | Canada
 Cassandra  | Beauty     | 2003-01-01 | Canada
 Barby      | Clothing   | 2016-12-26 | Canada
```

#### Generate a report that shows how the salary spending budget has fluctuated for every 90 day period.
```sql
SELECT hire_date, salary, 
(
  SELECT SUM(salary) FROM employees e2 
  WHERE e2.hire_date BETWEEN e1.hire_date - 90 AND e1.hire_date
) AS spending_90_day_pattern
FROM employees e1
ORDER BY hire_date;
```

#### Views (Virtual table) generated Via SQL Query
```sql
CREATE VIEW v_employee_info AS
SELECT first_name, email, salary, e.department, division, region country
FROM employees e 
INNER JOIN departments d ON e.department = d.department
INNER JOIN regions r ON e.region_id = r.region_id
WHERE e.email IS NOT NULL;
```

#### Inline Views
```sql
SELECT * 
FROM (SELECT * FROM departments) a;
```