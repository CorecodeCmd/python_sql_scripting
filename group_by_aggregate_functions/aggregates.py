import click

from beautifultable import BeautifulTable

from database.db_connection import DbHandler

table = BeautifulTable()
db = DbHandler()


query_commands = (
    {
        "QUERY_AGGREGATE_EMPLOYEE_SALARY": """
            SELECT department, COUNT(*) AS total_dept_employees, AVG(salary) AS dept_avg_salary, MIN(salary) AS dept_lowest_salary, MAX(salary) dept_highest_salary
            FROM employees 
            GROUP BY department
        """
    },
    {
        "QUERY_EMPLOYEE_EMAIL_DOMAINS": """
            SELECT SUBSTRING(email, POSITION('@' IN email) + 1) AS email_domain, COUNT(*) AS total_employees
            FROM employees
            WHERE email IS NOT NULL
            GROUP BY SUBSTRING(email, POSITION('@' IN email) + 1)
            ORDER BY COUNT(*) DESC 
            LIMIT 20
        """
    },
    {
        "QUERY_GENDER_REGIONAL_SALARY": """
            SELECT gender, region_id, MIN(salary) AS min_salary, MAX(salary) AS max_salary, ROUND(AVG(salary), 0) as avg_salary 
            FROM employees 
            GROUP BY gender, region_id 
            ORDER BY gender, region_id
        """
    },
)

sample_analytics_problems = (
    'Return the average salary, lowest salary, highest salary and the total count of employees working for each department',
    'Return employees and their counts according to their email domains(e.gmail.com, yahoo.com, tau.edu)',
    'Return a salary report showing employee gender, region id, lowest salary, highest salary average salary grouped by gender and regions',
)


def sql_aggregate_functions():
    
    with db.connection() as conn:
        with conn.cursor() as cursor:
            click.echo(click.style('SELECT FROM  THE QUERIES BELOW',
                                    fg='red', bold=True))
            menu = 'select_problem'
            while True:
                if menu == 'select_problem':
                    for index, value in enumerate(sample_analytics_problems):
                        click.echo(click.style(f"   {index + 1}:  {value}", fg="blue"))
                    click.echo(click.style("   BACK(b):  Return to Main Menu", fg="blue"))
                    char = click.getchar()
                    if char == '1':
                        click.clear()
                        sql = query_commands[0]['QUERY_AGGREGATE_EMPLOYEE_SALARY'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["department", "total_dept_employees", "dept_avg_salary", "dept_lowest_salary", "dept_highest_salary"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '2':
                        click.clear()
                        sql = query_commands[1]['QUERY_EMPLOYEE_EMAIL_DOMAINS'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["email_domain", "total_employees"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '3':
                        click.clear()
                        sql = query_commands[2]['QUERY_GENDER_REGIONAL_SALARY'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["gender", "region_id", "min_salary", "max_salary", "avg_salary"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == 'b':
                        click.clear()
                        menu = 'Back'
                elif menu == 'Back':
                    return