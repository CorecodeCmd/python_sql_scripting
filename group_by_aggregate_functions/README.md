# GROUPING DATA AND COMPUTING AGGREGATES

#### Return the total count of employees working for each department
```sql
SELECT department, COUNT(*) AS dept_employees 
FROM employees 
GROUP BY department;
```

#### Return the average salary, lowest salary, highest salary and the total count of employees working for each department
```sql
SELECT department, COUNT(*) AS total_dept_employees, AVG(salary) AS dept_avg_salary, MIN(salary) AS dept_lowest_salary, MAX(salary) dept_highest_salary
FROM employees 
GROUP BY department;
```


#### Return departments having greater than 35 employees working
```sql
SELECT department, COUNT(*) AS total_dept_employees 
FROM employees 
GROUP BY department
HAVING COUNT(*) > 35;
```

#### Return employees and their counts having the same first_name
```sql
SELECT first_name, COUNT(*) AS total_employees 
FROM employees 
GROUP BY first_name 
HAVING COUNT(*) > 1;
```


#### Return the distinct employee departments (Don’t use the Distinct Keyword)
```sql
SELECT department
FROM employees 
GROUP BY department;
```

#### Return employees and their counts according to their email domains(e.gmail.com, yahoo.com, tau.edu)
```sql
SELECT SUBSTRING(email, POSITION('@' IN email) + 1) AS email_domain, COUNT(*) AS total_employees
FROM employees
WHERE email IS NOT NULL
GROUP BY SUBSTRING(email, POSITION('@' IN email) + 1)
ORDER BY COUNT(*) DESC;
```

#### Return a salary report showing employee gender, region id, lowest salary, highest salary average salary grouped by gender and regions.
```sql
SELECT gender, region_id, MIN(salary) AS min_salary, MAX(salary) AS max_salary, ROUND(AVG(salary), 0) as avg_salary 
FROM employees 
GROUP BY gender, region_id 
ORDER BY gender, region_id;
```
