# SQL FUNCTIONS (UPPER(), LOWER(), LENGTH(), TRIM(), SUNSTRING(), REPLACE(), POSITION(), COALESCE(), MIN(), MAX(), AVG(), SUM(), COUNT())

#### Return all of the records and columns from professors but shorten the department names to only the first three characters in upper case
```sql
SELECT last_name, UPPER(SUBSTRING(department, 1, 3)) as department, salary, hire_date
FROM professors
```

#### Return the highest and lowest salary from professors excluding the professor named ‘Wilson’
```sql
SELECT MAX(salary) as highest_salary, MIN(salary) as lowest_salary
FROM professors
WHERE last_name != 'Wilson'
```

#### Return the hire date of the professor that has been teaching the longest
```sql
SELECT MIN(hire_date) 
FROM professors
```