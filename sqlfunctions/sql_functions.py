import click

from beautifultable import BeautifulTable

from database.db_connection import DbHandler

table = BeautifulTable()
db = DbHandler()


query_commands = (
    {
        "QUERY_UPPER_CASE_PROFESSORS": """
            SELECT last_name, UPPER(SUBSTRING(department, 1, 3)) as department, salary, hire_date
            FROM professors
        """
    },
    {
        "QUERY_HIGHEST_LOWEST_PROFESSOR_SALARY": """
            SELECT MAX(salary) as higest_salary, MIN(salary) as lowest_salary
            FROM professors
            WHERE last_name != 'Wilson'
        """
    },
    {
        "QUERY_LONGEST_TEACHING_PROFESSOR": """
            SELECT MIN(hire_date) 
            FROM professors
        """
    },
)

sample_analytics_problems = (
    'Return all of the records and columns from professors but shorten the department names to only the first three characters in upper case',
    'Return the highest and lowest salary from professors excluding the professor named ‘Wilson’',
    'Return the hire date of the professor that has been teaching the longest',
)


def sql_functions():
    
    with db.connection() as conn:
        with conn.cursor() as cursor:
            click.echo(click.style('SELECT FROM  THE QUERIES BELOW',
                                    fg='red', bold=True))
            menu = 'select_problem'
            while True:
                if menu == 'select_problem':
                    for index, value in enumerate(sample_analytics_problems):
                        click.echo(click.style(f"   {index + 1}:  {value}", fg="blue"))
                    click.echo(click.style("   BACK(b):  Return to Main Menu", fg="blue"))
                    char = click.getchar()
                    if char == '1':
                        click.clear()
                        sql = query_commands[0]['QUERY_UPPER_CASE_PROFESSORS'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["last_name", "department", "salary", "hire_date"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '2':
                        click.clear()
                        sql = query_commands[1]['QUERY_HIGHEST_LOWEST_PROFESSOR_SALARY'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["highest_salary", "lowest_salary"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == '3':
                        click.clear()
                        sql = query_commands[2]['QUERY_LONGEST_TEACHING_PROFESSOR'].strip()
                        cursor.execute(sql)
                        rows = cursor.fetchall()
                        table.column_headers = ["longest_hire_date"]
                        for row in rows:
                            table.append_row(row)
                        click.echo(table)
                    elif char == 'b':
                        click.clear()
                        menu = 'Back'
                elif menu == 'Back':
                    return