from setuptools import setup, find_packages

setup(
    name="SQL TOOL",
    version="1.0",
    py_modules=["sql_tool"],
    # packages=find_packages(),
    # include_package_data=True,
    install_requires=[
        "click",
        "psycopg2-binary",
        "beautifultable"
    ],
    entry_points="""
        [console_scripts]
        sql_tool=sql_tool:cli
    """,
)
