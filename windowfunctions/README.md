# WINDOW FUNCTIONS (Combining grouping columns with non-grouping columns)

#### Using aggregates with non-aggregate fields some times yields unexpected results
```sql
SELECT first_name, department, COUNT(*) emp_no
FROM employees e
GROUP BY department, first_name;

first_name   |    department     | emp_no
----------------+-------------------+--------
 Benjamen       | Pharmacy          |      1
 Marlane        | First Aid         |      1
 Jonathan       | Furniture         |      1
 Archambault    | Vitamins          |      1
 Howey          | Books             |      1
 Jayme          | Camping           |      1
 Cornelle       | Books             |      1
 Valdemar       | Cosmetics         |      1
 Alfy           | Games             |      1
 Emanuele       | Garden            |      1
 Isis           | Movies            |      1
```

#### Using Correlated subquery(Expensive query)
```sql
SELECT first_name, department, 
(SELECT COUNT(*) FROM employees e1 WHERE e1.department = e.department) emp_no
FROM employees e
GROUP BY department, first_name;

first_name   |    department     | emp_no
----------------+-------------------+--------
 Benjamen       | Pharmacy          |     38
 Marlane        | First Aid         |     58
 Jonathan       | Furniture         |     43
 Archambault    | Vitamins          |     37
 Howey          | Books             |     37
 Jayme          | Camping           |     36
 Cornelle       | Books             |     37
 Valdemar       | Cosmetics         |     34
```

#### Using Window function for the above. OVER()
 … OVER() with empty paranthensis returns overall total employees
```sql
SELECT first_name, department, 
COUNT(*) OVER(PARTITION BY department)
FROM employees

 first_name   |    department     | count
----------------+-------------------+-------
 Lorelle        | Automotive        |    32
 Sterling       | Automotive        |    32
 Roslyn         | Automotive        |    32
 Abbott         | Automotive        |    32
 Cybil          | Automotive        |    32
 Mill           | Automotive        |    32
 Maryellen      | Automotive        |    32
 Lauretta       | Automotive        |    32
```

#### With where clause
```sql
SELECT first_name, department, COUNT(*) OVER()    -- comment in sql
FROM employees
WHERE region_id = 3;

first_name  |    department     | count
-------------+-------------------+-------
 Aeriell     | Tools             |   145
 Bethena     | Sports            |   145
 Seline      | Phones & Tablets  |   145
 Leonora     | Beauty            |   145
 Bernardine  | Device Repair     |   145
 Cayla       | Grocery           |   145
```

```sql
SELECT first_name, department, COUNT(*) OVER(PARTITION BY department)    -- comment in sql
FROM employees
WHERE region_id = 3;

first_name  |    department     | count
-------------+-------------------+-------
 Charis      | Automotive        |     2
 Jilleen     | Automotive        |     2
 Aguie       | Beauty            |     9
 Olenolin    | Beauty            |     9
 Alvin       | Beauty            |     9
 Efrem       | Beauty            |     9
 Reg         | Beauty            |     9
 Baxter      | Beauty            |     9
 Fulton      | Beauty            |     9
 Leonora     | Beauty            |     9
 Evey        | Beauty            |     9
 Erich       | Books             |    12
 Howey       | Books             |    12
 Olvan       | Books             |    12
 Del         | Books             |    12
 Clarance    | Books             |    12
 Shaina      | Books             |    12
 Glory       | Books             |    12
 Lydon       | Books             |    12
```

#### Using window function with multiple columns
```sql
SELECT first_name, department, 
COUNT(*) OVER(PARTITION BY department) dept_count,
region_id,
COUNT(*) OVER(PARTITION BY region_id) region_count
FROM employees
```

##### Salary by department
```sql
SELECT first_name, department, 
SUM(salary) OVER(PARTITION BY department) total_dept_salary
FROM employees
```

### FRAMING (Ordering Data in Window Frames)
Getting running salary total according to hire_date
```sql
SELECT first_name, hire_date, salary, 
SUM(salary) OVER(ORDER BY hire_date RANGE BETWEEN UNBOUNDED PRECEDING 
                 AND CURRENT ROW) as running_total_of_salaries
FROM employees;
```

OR (Same as above)

```sql
SELECT first_name, hire_date, salary, 
SUM(salary) OVER(ORDER BY hire_date) as running_total_of_salaries
FROM employees;

first_name | hire_date  | salary | running_total_of_salaries
------------+------------+--------+---------------------------
 Norbie     | 2003-01-01 |  82215 |                    189151
 Cassandra  | 2003-01-01 | 106936 |                    189151
 Rora       | 2003-01-12 | 153489 |                    342640
 Feliks     | 2003-01-14 |  55307 |                    397947
 Cecilius   | 2003-01-20 |  98882 |                    496829
 Eugenius   | 2003-01-26 | 152118 |                    648947
 Fiorenze   | 2003-02-17 |  51266 |                    700213
 Elnora     | 2003-02-22 |  34355 |                    734568
 Chelsey    | 2003-02-24 |  57309 |                    791877
 Dorothea   | 2003-02-27 |  46062 |                    837939
 Yardley    | 2003-02-28 |  26876 |                    864815
 Dayle      | 2003-03-01 |  82753 |                    947568
 Bear       | 2003-03-02 | 143117 |                   1090685
 Nonnah     | 2003-03-08 | 145169 |                   1235854
 Jamie      | 2003-03-13 |  99770 |                   1335624
 Arvy       | 2003-03-16 |  76125 |                   1411749
 Edna       | 2003-04-09 |  91397 |                   1651677
 Alis       | 2003-04-09 | 148531 |                   1651677
 Maryellen  | 2003-04-19 | 115973 |                   1767650
```

#### Framing by adjacent rows
```sql
SELECT first_name, hire_date, salary, 
SUM(salary) OVER(ORDER BY hire_date ROWS BETWEEN 1 PRECEDING    -- Adding salaries by preceding rows(1 add by 1 preceding row, 2 add by 2 preceding rows)
                 AND CURRENT ROW) as running_total_of_salaries
FROM employees;

  first_name   | hire_date  | salary | running_total_of_salaries
----------------+------------+--------+---------------------------
 Norbie         | 2003-01-01 |  82215 |                     82215
 Cassandra      | 2003-01-01 | 106936 |                    189151
 Rora           | 2003-01-12 | 153489 |                    260425
 Feliks         | 2003-01-14 |  55307 |                    208796
 Cecilius       | 2003-01-20 |  98882 |                    154189
 Eugenius       | 2003-01-26 | 152118 |                    251000
 Fiorenze       | 2003-02-17 |  51266 |                    203384
 Elnora         | 2003-02-22 |  34355 |                     85621
 Chelsey        | 2003-02-24 |  57309 |                     91664
 Dorothea       | 2003-02-27 |  46062 |                    103371
 Yardley        | 2003-02-28 |  26876 |                     72938
 Dayle          | 2003-03-01 |  82753 |                    109629
 Bear           | 2003-03-02 | 143117 |                    225870
 Nonnah         | 2003-03-08 | 145169 |                    288286
 Jamie          | 2003-03-13 |  99770 |                    244939
 Arvy           | 2003-03-16 |  76125 |                    175895
 Edna           | 2003-04-09 |  91397 |                    167522
 Alis           | 2003-04-09 | 148531 |                    239928
 Maryellen      | 2003-04-19 | 115973 |                    264504
 Dorene         | 2003-04-23 |  97700 |                    213673
 Archibold      | 2003-04-26 |  69379 |                    167079
```

### Framing and Partition
```sql
SELECT first_name, department, hire_date, salary, 
SUM(salary) OVER(PARTITION BY department ORDER BY hire_date) as running_total_of_salaries
FROM employees;

  first_name   |    department     | hire_date  | salary | running_total_of_salaries
----------------+-------------------+------------+--------+---------------------------
 Maryellen      | Automotive        | 2003-04-19 | 115973 |                    115973
 Archibold      | Automotive        | 2003-04-26 |  69379 |                    185352
 Tabb           | Automotive        | 2003-05-02 |  47591 |                    232943
 Abbott         | Automotive        | 2003-06-05 | 106517 |                    339460
 Ladonna        | Automotive        | 2003-08-10 | 111775 |                    451235
 Roslyn         | Automotive        | 2003-08-11 | 157260 |                    608495
 Lorelle        | Automotive        | 2004-01-27 | 119959 |                    728454
 Cybil          | Automotive        | 2004-04-01 | 123828 |                    852282
 Sterling       | Automotive        | 2004-09-02 |  56095 |                    908377
 Jessalyn       | Automotive        | 2004-10-03 |  86929 |                    995306
 Irita          | Automotive        | 2004-10-15 | 160783 |                   1156089
 Way            | Automotive        | 2006-07-15 |  51989 |                   1208078
 Laurie         | Automotive        | 2007-05-31 |  29752 |                   1237830
 Berkley        | Automotive        | 2007-06-11 |  44641 |                   1282471
 Lauretta       | Automotive        | 2007-12-14 |  75978 |                   1358449
 Merlina        | Automotive        | 2009-02-16 | 137094 |                   1495543
 Ivie           | Automotive        | 2010-04-04 | 111359 |                   1606902
 Poppy          | Automotive        | 2010-07-27 | 144511 |                   1751413
 Mill           | Automotive        | 2011-01-08 | 162522 |                   1913935
 Katlin         | Automotive        | 2011-05-21 |  60089 |                   1974024
 Tammie         | Automotive        | 2011-12-08 | 160039 |                   2134063
 Betsey         | Automotive        | 2012-02-10 | 152141 |                   2286204
 Jilleen        | Automotive        | 2013-04-20 | 137393 |                   2423597
 Vanda          | Automotive        | 2014-02-06 | 103570 |                   2527167
 Chrissy        | Automotive        | 2014-02-27 | 146522 |                   2673689
 Noelyn         | Automotive        | 2014-05-28 | 125305 |                   2798994
 Doe            | Automotive        | 2014-10-14 | 106046 |                   2905040
 Stormy         | Automotive        | 2015-07-28 | 126983 |                   3032023
 Charis         | Automotive        | 2015-08-14 | 130995 |                   3163018
 Cy             | Automotive        | 2016-01-18 | 144146 |                   3307164
 Clementina     | Automotive        | 2016-09-04 |  95492 |                   3402656
 Cherianne      | Automotive        | 2016-12-18 | 150821 |                   3553477
 Cassandra      | Beauty            | 2003-01-01 | 106936 |                    106936
 Willabella     | Beauty            | 2003-08-18 |  22053 |                    128989
 Olenolin       | Beauty            | 2004-09-29 | 137691 |                    266680
 Lukas          | Beauty            | 2004-11-03 |  63076 |                    329756
 Rhianna        | Beauty            | 2005-04-07 | 120753 |                    450509
 Kathye         | Beauty            | 2005-10-09 |  71413 |                    521922
 Eldredge       | Beauty            | 2006-04-30 |  60077 |                    581999
 Garrot         | Beauty            | 2006-07-10 |  28293 |                    610292
 Baxter         | Beauty            | 2006-07-18 | 154018 |                    764310
 De witt        | Beauty            | 2006-08-30 |  74511 |                    838821
 Efrem          | Beauty            | 2006-09-22 |  56615 |                    895436
 Lacee          | Beauty            | 2006-11-27 | 111691 |                   1007127
 Deina          | Beauty            | 2007-03-24 | 101678 |                   1108805
 Neddy          | Beauty            | 2007-04-26 | 151926 |                   1260731
 Jobina         | Beauty            | 2007-07-03 | 119587 |                   1380318
 Orland         | Beauty            | 2007-07-12 | 162845 |                   1543163
```

### RANKING

#### Rank employee salaries according to department
```sql
SELECT first_name, department, salary, 
RANK() OVER(PARTITION BY department ORDER BY salary DESC) 
FROM employees;

first_name   |    department     | salary | rank
----------------+-------------------+--------+------
 Mill           | Automotive        | 162522 |    1
 Irita          | Automotive        | 160783 |    2
 Tammie         | Automotive        | 160039 |    3
 Roslyn         | Automotive        | 157260 |    4
 Betsey         | Automotive        | 152141 |    5
 Cherianne      | Automotive        | 150821 |    6
 Chrissy        | Automotive        | 146522 |    7
 Poppy          | Automotive        | 144511 |    8
 Cy             | Automotive        | 144146 |    9
 Jilleen        | Automotive        | 137393 |   10
 Merlina        | Automotive        | 137094 |   11
 Charis         | Automotive        | 130995 |   12
 Stormy         | Automotive        | 126983 |   13
 Noelyn         | Automotive        | 125305 |   14
 Cybil          | Automotive        | 123828 |   15
 Lorelle        | Automotive        | 119959 |   16
 Maryellen      | Automotive        | 115973 |   17
 Ladonna        | Automotive        | 111775 |   18
 Ivie           | Automotive        | 111359 |   19
 Abbott         | Automotive        | 106517 |   20
 Doe            | Automotive        | 106046 |   21
 Vanda          | Automotive        | 103570 |   22
 Clementina     | Automotive        |  95492 |   23
 Jessalyn       | Automotive        |  86929 |   24
 Lauretta       | Automotive        |  75978 |   25
 Archibold      | Automotive        |  69379 |   26
 Katlin         | Automotive        |  60089 |   27
 Sterling       | Automotive        |  56095 |   28
 Way            | Automotive        |  51989 |   29
 Tabb           | Automotive        |  47591 |   30
 Berkley        | Automotive        |  44641 |   31
 Laurie         | Automotive        |  29752 |   32
 Orland         | Beauty            | 162845 |    1
 Urban          | Beauty            | 162169 |    2
 Baxter         | Beauty            | 154018 |    3
 Neddy          | Beauty            | 151926 |    4
 Garald         | Beauty            | 145225 |    5
```

#### Return Employees in their 8th position per department salaries
```sql
SELECT * FROM (
SELECT first_name, department, salary, 
RANK() OVER(PARTITION BY department ORDER BY salary DESC) 
FROM employees
) a
WHERE rank = 8;

 first_name |    department     | salary | rank
------------+-------------------+--------+------
 Poppy      | Automotive        | 144511 |    8
 Murray     | Beauty            | 138181 |    8
 Akim       | Books             | 139254 |    8
 Izabel     | Camping           | 142059 |    8
 Coralie    | Children Clothing | 128326 |    8
 Kippie     | Clothing          | 128327 |    8
 Erminia    | Computers         | 146467 |    8
 Stanwood   | Cosmetics         | 152382 |    8
 Shelton    | Decor             | 139827 |    8
 Gustave    | Device Repair     | 147926 |    8
 Donovan    | First Aid         | 139035 |    8
 Creigh     | Furniture         | 133537 |    8
 Wendel     | Games             | 118392 |    8
 Thorpe     | Garden            | 121211 |    8
 Roobbie    | Grocery           | 146639 |    8
 Maye       | Jewelry           | 116120 |    8
 Selle      | Maintenance       |  40041 |    8
 Dionysus   | Movies            | 145973 |    8
 Horst      | Music             | 130342 |    8
 Emlyn      | Pharmacy          | 146046 |    8
 Brandice   | Phones & Tablets  | 134058 |    8
 Darrin     | Sports            | 142943 |    8
 Shem       | Tools             | 121211 |    8
 Nadine     | Toys              | 141032 |    8
 Cary       | Vitamins          | 139145 |    8
```

### NTILE

#### Breaking department salary in 5 groups(brackets) and ranking it
```sql
SELECT first_name, department, salary, 
NTILE(5) OVER(PARTITION BY department ORDER BY salary DESC) salary_bracket
FROM employees

   first_name   |    department     | salary | salary_bracket
----------------+-------------------+--------+----------------
 Mill           | Automotive        | 162522 |              1
 Irita          | Automotive        | 160783 |              1
 Tammie         | Automotive        | 160039 |              1
 Roslyn         | Automotive        | 157260 |              1
 Betsey         | Automotive        | 152141 |              1
 Cherianne      | Automotive        | 150821 |              1
 Chrissy        | Automotive        | 146522 |              1
 Poppy          | Automotive        | 144511 |              2
 Cy             | Automotive        | 144146 |              2
 Jilleen        | Automotive        | 137393 |              2
 Merlina        | Automotive        | 137094 |              2
 Charis         | Automotive        | 130995 |              2
 Stormy         | Automotive        | 126983 |              2
 Noelyn         | Automotive        | 125305 |              2
 Cybil          | Automotive        | 123828 |              3
 Lorelle        | Automotive        | 119959 |              3
 Maryellen      | Automotive        | 115973 |              3
 Ladonna        | Automotive        | 111775 |              3
 Ivie           | Automotive        | 111359 |              3
 Abbott         | Automotive        | 106517 |              3
 Doe            | Automotive        | 106046 |              4
 Vanda          | Automotive        | 103570 |              4
 Clementina     | Automotive        |  95492 |              4
 Jessalyn       | Automotive        |  86929 |              4
 Lauretta       | Automotive        |  75978 |              4
 Archibold      | Automotive        |  69379 |              4
 Katlin         | Automotive        |  60089 |              5
 Sterling       | Automotive        |  56095 |              5
 Way            | Automotive        |  51989 |              5
 Tabb           | Automotive        |  47591 |              5
 Berkley        | Automotive        |  44641 |              5
 Laurie         | Automotive        |  29752 |              5
```

#### FIRST_VALUE(field) AND nth_VALUE(field, num) e.g
```sql
SELECT first_name, department, salary, 
FIRST_VALUE(salary) OVER(PARTITION BY department ORDER BY salary DESC) salary_bracket
FROM employees

first_name   |    department     | salary | salary_bracket
----------------+-------------------+--------+----------------
 Mill           | Automotive        | 162522 |         162522
 Irita          | Automotive        | 160783 |         162522
 Tammie         | Automotive        | 160039 |         162522
 Roslyn         | Automotive        | 157260 |         162522
 Betsey         | Automotive        | 152141 |         162522
 Cherianne      | Automotive        | 150821 |         162522
 Chrissy        | Automotive        | 146522 |         162522
 Poppy          | Automotive        | 144511 |         162522
 Cy             | Automotive        | 144146 |         162522
 Jilleen        | Automotive        | 137393 |         162522
```

```sql
SELECT first_name, department, salary, 
NTH_VALUE(salary, 5) OVER(PARTITION BY department ORDER BY salary DESC) salary_bracket
FROM employees

first_name   |    department     | salary | salary_bracket
----------------+-------------------+--------+----------------
 Mill           | Automotive        | 162522 |
 Irita          | Automotive        | 160783 |
 Tammie         | Automotive        | 160039 |
 Roslyn         | Automotive        | 157260 |
 Betsey         | Automotive        | 152141 |         152141
 Cherianne      | Automotive        | 150821 |         152141
 Chrissy        | Automotive        | 146522 |         152141
```

### LEAD AND LAG
```sql
SELECT first_name, salary, 
LEAD(salary) OVER() next_salary 
FROM employees

first_name   | salary | next_salary
----------------+--------+-------------
 Berrie         | 154864 |       56752
 Aeriell        |  56752 |       95313
 Sydney         |  95313 |      119674
 Avrom          | 119674 |       55307
 Feliks         |  55307 |      134501
 Bethena        | 134501 |       28995
```

```sql
SELECT first_name, salary, 
LAG(salary) OVER() previous_salary 
FROM employees;

first_name   | salary | previous_salary
----------------+--------+-----------------
 Berrie         | 154864 |
 Aeriell        |  56752 |          154864
 Sydney         |  95313 |           56752
 Avrom          | 119674 |           95313
 Feliks         |  55307 |          119674
 Bethena        | 134501 |           55307
 Ardeen         |  28995 |          134501
 Seline         | 101066 |           28995
 Dayle          |  82753 |          101066
```

```sql
SELECT first_name, salary, 
LAG(salary) OVER(ORDER BY salary DESC) closest_higher_salary 
FROM employees;

first_name   | salary | closest_higher_salary
----------------+--------+-----------------------
 Jacklyn        | 166976 |
 Carissa        | 166765 |                166976
 Riley          | 166569 |                166765
 Lauren         | 166016 |                166569
 Lucy           | 165660 |                166016
 Barby          | 164588 |                165660
 Ev             | 164582 |                164588
 Sherwynd       | 164470 |                164582
 Michail        | 164355 |                164470
 Hermione       | 164219 |                164355
```

```sql
SELECT first_name, salary, 
LEAD(salary) OVER(PARTITION BY department ORDER BY salary DESC) closest_lower_salary 
FROM employees;

first_name   | salary | closest_lower_salary
----------------+--------+----------------------
 Mill           | 162522 |               160783
 Irita          | 160783 |               160039
 Tammie         | 160039 |               157260
 Roslyn         | 157260 |               152141
 Betsey         | 152141 |               150821
 Cherianne      | 150821 |               146522
 Chrissy        | 146522 |               144511
```

### ROLLUPS AND CUBES
```sql
SELECT continent, country, city, SUM(units_sold)
FROM sales
GROUP BY GROUPING SETS(continent, country, city);

  continent   | country |    city    |  sum
---------------+---------+------------+-------
 Asia          |         |            | 15000
 North America |         |            | 30000
 Europe        |         |            | 23000
               | France  |            |  5000
               | China   |            | 10000
               | UK      |            | 18000
               | Canada  |            | 30000
               | Japan   |            |  5000
               |         | Montreal   |  5000
               |         | Hong Kong  |  7000
               |         | Shanghai   |  3000
               |         | Vancouver  | 15000
               |         | Paris      |  5000
               |         | Tokyo      |  5000
               |         | Manchester | 12000
               |         | London     |  6000
               |         | Toronto    | 10000
```

```sql
SELECT continent, country, city, SUM(units_sold)
FROM sales
GROUP BY GROUPING SETS(continent, country, city, ());  ---first row returned is the grand total

continent   | country |    city    |  sum
---------------+---------+------------+-------
               |         |            | 68000
 Asia          |         |            | 15000
 North America |         |            | 30000
 Europe        |         |            | 23000
               | France  |            |  5000
               | China   |            | 10000
               | UK      |            | 18000
               | Canada  |            | 30000
               | Japan   |            |  5000
               |         | Montreal   |  5000
               |         | Hong Kong  |  7000
               |         | Shanghai   |  3000
               |         | Vancouver  | 15000
               |         | Paris      |  5000
               |         | Tokyo      |  5000
               |         | Manchester | 12000
               |         | London     |  6000
               |         | Toronto    | 10000
```

```sql
SELECT continent, country, city, SUM(units_sold)  --first row returned is the grand total
FROM sales
GROUP BY ROLLUP(continent, country, city);

   continent   | country |    city    |  sum
---------------+---------+------------+-------
               |         |            | 68000
 Asia          | Japan   | Tokyo      |  5000
 North America | Canada  | Montreal   |  5000
 Europe        | France  | Paris      |  5000
 Europe        | UK      | London     |  6000
 North America | Canada  | Toronto    | 10000
 Asia          | China   | Shanghai   |  3000
 Asia          | China   | Hong Kong  |  7000
 Europe        | UK      | Manchester | 12000
 North America | Canada  | Vancouver  | 15000
 Asia          | Japan   |            |  5000
 Asia          | China   |            | 10000
 Europe        | UK      |            | 18000
 Europe        | France  |            |  5000
 North America | Canada  |            | 30000
 Asia          |         |            | 15000
 North America |         |            | 30000
 Europe        |         |            | 23000
```

```sql
SELECT continent, country, city, SUM(units_sold)  --first row returned is the grand total
FROM sales
GROUP BY CUBE(continent, country, city);

  continent   | country |    city    |  sum
---------------+---------+------------+-------
               |         |            | 68000
 Asia          | Japan   | Tokyo      |  5000
 North America | Canada  | Montreal   |  5000
 Europe        | France  | Paris      |  5000
 Europe        | UK      | London     |  6000
 North America | Canada  | Toronto    | 10000
 Asia          | China   | Shanghai   |  3000
 Asia          | China   | Hong Kong  |  7000
 Europe        | UK      | Manchester | 12000
 North America | Canada  | Vancouver  | 15000
 Asia          | Japan   |            |  5000
 Asia          | China   |            | 10000
 Europe        | UK      |            | 18000
 Europe        | France  |            |  5000
 North America | Canada  |            | 30000
 Asia          |         |            | 15000
 North America |         |            | 30000
 Europe        |         |            | 23000
               | China   | Hong Kong  |  7000
               | Canada  | Montreal   |  5000
               | Japan   | Tokyo      |  5000
               | China   | Shanghai   |  3000
               | Canada  | Toronto    | 10000
               | Canada  | Vancouver  | 15000
               | UK      | Manchester | 12000
               | UK      | London     |  6000
               | France  | Paris      |  5000
               | France  |            |  5000
               | China   |            | 10000
               | UK      |            | 18000
               | Canada  |            | 30000
               | Japan   |            |  5000
 North America |         | Vancouver  | 15000
 Asia          |         | Hong Kong  |  7000
 Asia          |         | Shanghai   |  3000
 Europe        |         | London     |  6000
 Asia          |         | Tokyo      |  5000
 Europe        |         | Manchester | 12000
 North America |         | Toronto    | 10000
 North America |         | Montreal   |  5000
 Europe        |         | Paris      |  5000
               |         | Montreal   |  5000
               |         | Hong Kong  |  7000
               |         | Shanghai   |  3000
               |         | Vancouver  | 15000
               |         | Paris      |  5000
               |         | Tokyo      |  5000
               |         | Manchester | 12000
               |         | London     |  6000
               |         | Toronto    | 10000
```